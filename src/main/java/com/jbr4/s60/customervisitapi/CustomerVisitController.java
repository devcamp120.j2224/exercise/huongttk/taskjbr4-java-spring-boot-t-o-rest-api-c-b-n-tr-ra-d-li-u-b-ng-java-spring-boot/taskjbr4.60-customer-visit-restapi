package com.jbr4.s60.customervisitapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class CustomerVisitController {
    @CrossOrigin
    @GetMapping("/visit")
    public ArrayList<Visit> getVisitList(){
        ArrayList<Visit> listVisit = new ArrayList<>();
        Customer customer1 = new Customer("Huong");
        Customer customer2 = new Customer("Duy");
        Customer customer3 = new Customer("Son");

        
        System.out.println("customer1 = " + customer1);
        System.out.println("customer2 = " + customer2);
        System.out.println("customer3 = " + customer3);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());

        System.out.println("visit1 = " + visit1);
        System.out.println("visit2 = " + visit2);
        System.out.println("visit3 = " + visit3);

        listVisit.add(visit1);
        listVisit.add(visit2);
        listVisit.add(visit3);

        return listVisit;
    }
}
