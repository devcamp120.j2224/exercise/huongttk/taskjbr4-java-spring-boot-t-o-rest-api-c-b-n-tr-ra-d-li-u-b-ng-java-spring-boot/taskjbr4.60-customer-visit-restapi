package com.jbr4.s60.customervisitapi;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date = new Date();
    private double serviceExpense;
    private double productExpense;

    
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }


    public Customer getCustomer() {
        return customer;
    }


    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    public Date getDate() {
        return date;
    }


    public void setDate(Date date) {
        this.date = date;
    }


    public double getServiceExpense() {
        return serviceExpense;
    }


    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }


    public double getProductExpense() {
        return productExpense;
    }


    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }


    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpense=" + productExpense
                + ", serviceExpense=" + serviceExpense + "]";
    }

    

}
