package com.jbr4.s60.customervisitapi;

public class Customer {
    private String name = "";
    private boolean mamber = false;
    private String mamberType = "";

    
    public Customer(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public boolean isMamber() {
        return mamber;
    }


    public void setMamber(boolean mamber) {
        this.mamber = mamber;
    }


    public String getMamberType() {
        return mamberType;
    }


    public void setMamberType(String mamberType) {
        this.mamberType = mamberType;
    }


    @Override
    public String toString() {
        return "Customer [mamber=" + mamber + ", mamberType=" + mamberType + ", name=" + name + "]";
    }


    
}
